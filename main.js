document.getElementById("send-button").onclick = getFormInfo;
document.getElementById("clear-button").onclick = removeResponses;

function removeResponses(){
    document.getElementById("response-label").innerHTML = "";
    document.getElementById("extrapolated-response-label").innerHTML = "";
}

function getFormInfo() {
    console.log("entered getFormInfo");
    var sel_index = document.getElementById("select-server-address").selectedIndex;
    var url_base = document.getElementById("select-server-address").options[sel_index].value;
    var port = document.getElementById("input-port-number").value;
    if (port == null) {
        alert("Port is required");
        return;
    }
    url_base = url_base + ":" + port;
    var msg_type = ""
    if (document.getElementById("radio-get").checked) {
        msg_type = "GET";
    }
    else if (document.getElementById("radio-put").checked) {
        msg_type = "PUT";
    }
    else if (document.getElementById("radio-post").checked) {
        msg_type = "POST"
    }
    else if (document.getElementById("radio-delete").checked) {
        msg_type = "DELETE";
    }
    var key = null;
    if (document.getElementById("checkbox-use-key").checked) {
        key = document.getElementById("input-key").value;
    }
    var msg_body = null;
    if (use_msg_body = document.getElementById("checkbox-use-message").checked) {
        msg_body = document.getElementById("text-message-body").value;
    }
    var form_info = {};
    form_info.url_base = url_base;
    form_info.msg_type = msg_type;
    form_info.key = key;
    form_info.msg_body = msg_body;
    makeRequest(form_info);
}

function makeRequest(form_info) {
    console.log("entered makeRequest");
    console.log(form_info);

    var xhr = new XMLHttpRequest();
    var url = "";

    switch(form_info.msg_type) {
        case "GET":
            // should be no body
            if (form_info.msg_body != null){
                alert("No body allowed for GET requests.");
                return;
            }
            if (document.getElementById("checkbox-use-message").checked == true) {
                alert("Please uncheck 'Use Message Body' if you would like to send a GET request.");
            }
            // check for key
            if (form_info.key == null) { // no key
                var key_value_entered = document.getElementById("input-key").value;
                if (key_value_entered) {
                    alert("Did you want to use a key value?");
                    break;
                }
                url = form_info.url_base + "/movies/";
                xhr.open("GET", url, true);
            }
            else { // key
                var key_value_entered = document.getElementById("input-key").value;
                if (key_value_entered == null) {
                    alert("Did you want to enter a key value?");
                    break;
                }
                url = form_info.url_base + "/movies/" + form_info.key;
                xhr.open("GET", url, true);
            }
            break;
        case "PUT":
            // should have a key
            if (form_info.key == null) {
                alert("PUT requests require a key");
                return;
            }
            // should have a body
            if (form_info.msg_body == null) {
                alert("PUT requests require a body");
                return;
            }
            if (document.getElementById("checkbox-use-message").checked == false) {
                alert("Please check 'Use Message Body' if you would like to send a message body.");
            }
            url = form_info.url_base + "/movies/" + form_info.key;
            xhr.open("PUT", url, true);
            break;
        case "POST":
            // should have no key
            if (form_info.key != null) {
                alert("No key allowed for POST requests");
                return;
            }
            // should have a body
            if (form_info.msg_body == null) {
                alert("POST request requires a body");
                return;
            }
            if (document.getElementById("checkbox-use-message").checked == false) {
                alert("Please check 'Use Message Body' if you would like to send a message body.");
            }
            url = form_info.url_base + "/movies/";
            xhr.open("POST", url, true);
            break;
        case "DELETE":
            // should have a body
            if (form_info.msg_body == null) {
                alert("DELETE request requires a body of '{}'");
                return;
            }
            if (document.getElementById("checkbox-use-message").checked == false) {
                alert("Please check 'Use Message Body' if you would like to send a message body.");
            }
            // check for key
            // key
            if (form_info.key != null) {
                var key_value_entered = document.getElementById("input-key").value;
                if (key_value_entered == null) {
                    alert("Did you want to enter a key value?");
                    break;
                }
                url = form_info.url_base + "/movies/" + form_info.key;
                xhr.open("DELETE", url, true);
            }
            // no key
            else {
                var key_value_entered = document.getElementById("input-key").value;
                if (key_value_entered) {
                    alert("Did you want to use a key value?");
                    break;
                }
                url = form_info.url_base + "/movies/";
                xhr.open("DELETE", url, true);
            }
            break;
    }

    // what to do if request is successful
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        // update the response section with the response text from the server. Send
        // the request type and key information so the response section knows what type
        // of request was made
        updateResponseSection(xhr.responseText, form_info.msg_type, form_info.msg_body, form_info.key, false);
    }

    // what to do if request is not successful
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
        updateResponseSection(xhr.statusText, form_info.msg_type, form_info.msg_body, form_info.msg_body, true);
    }

    // actually make the network call
    if (form_info.msg_body == null) {
        xhr.send(null);
    }
    else {
        xhr.send(form_info.msg_body);
    }
}

function updateResponseSection(response_or_status_text, msg_type, msg_body, key, error) {
    console.log("entered updateResponseSection");
    // update response-label with pure response from server
    document.getElementById("response-label").innerHTML = response_or_status_text;
    var extrapolated_response = document.getElementById("extrapolated-response-label");
    var response_json = JSON.parse(response_or_status_text);
    msg_body = JSON.parse(msg_body);

    // request itself did not have an error
    if (!error) {
        switch (msg_type) {
            case "GET":
                // server error
                if (response_json["result"] == "error"){
                    extrapolated_response.innerHTML = "Error: " + response_json["message"];
                    return;
                }
                // no key
                if (key == null) {
                    for (movie of response_json["movies"]) {
                        extrapolated_response.innerHTML += movie["title"] + " belongs to the genre(s): " + movie["genres"] + "<br>";
                    }
                }
                // key
                else {
                    extrapolated_response.innerHTML = response_json["title"] + " belongs to the genre(s): " + response_json["genres"];
                }
                break;
            case "PUT":
                // server error
                if (response_json["result"] == "error"){
                    extrapolated_response.innerHTML = "Error: " + response_json["message"];
                    return;
                }
                // key
                extrapolated_response.innerHTML = "Movie with ID " + key + " was updated. New title: " + msg_body["title"] + ", new genre(s): " + msg_body["genres"];
                break;
            case "POST":
                // server error
                if (response_json["result"] == "error"){
                    extrapolated_response.innerHTML = "Error: " + response_json["message"];
                    return;
                }
                extrapolated_response.innerHTML = "Movie with title: " + msg_body["title"] + " and genre(s): " + msg_body["genres"] +  " created successfully. Its ID is: " + response_json["id"];
                break;
            case "DELETE":
                // server error
                if (response_json["result"] == "error"){
                    extrapolated_response.innerHTML = "Error: " + response_json["message"];
                    return;
                }
                // no key
                if (key == null) {
                    extrapolated_response.innerHTML = "All movies deleted successfully.";
                }
                // key
                else {
                    extrapolated_response.innerHTML = "Movie with ID: " + key + " deleted successfully";
                }
                break;
        }

    }
    // error with the request itself
    else {
        extrapolated_response.innerHTML = "Error with front-end request.";
    }
    

}