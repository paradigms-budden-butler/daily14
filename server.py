'''Bradley Budden (bbudden) and Megan Butler (mbutle13)'''

import cherrypy
import routes
from moviesController import MovieController
from ratingsController import RatingsController
from resetController import ResetController
from movies_library import _movie_database # why do we need this - to share the same instance of the mdb object

class OptionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"]="*"
    cherrypy.response.headers["Access-Control-Allow-Methods"]="GET,PUT,POST,DELETE,OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"]="true"


def start_service():
    '''configures and runs the server'''

    # Create the movie database object here so the controllers can all have the same one
    mdb = _movie_database() # from movie_library.py
    movieController = MovieController(mdb=mdb)
    resetController = ResetController(mdb=mdb)
    ratingsController = RatingsController(mdb=mdb)
    optionsController = OptionsController()

    dispatcher = cherrypy.dispatch.RoutesDispatcher() # we will use this to connect endpoints to controllers
    
    # connnecting endpoints to resources
    #connect(out_tag, http resource, class object with handler, event handler name, what type of HTTP request to serve) 
    
    # movieController
    dispatcher.connect('movie_get_key', '/movies/:movie_id', controller=movieController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('movie_put_key', '/movies/:movie_id', controller=movieController, action='PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('movie_delete_key', '/movies/:movie_id', controller=movieController, action='DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('movie_get_index', '/movies/', controller=movieController, action='GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('movie_post_index', '/movies/', controller=movieController, action='POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('movie_delete_index', '/movies/', controller=movieController, action='DELETE_INDEX', conditions=dict(method=['DELETE']))
    
    # resetController
    dispatcher.connect('reset_put_index', '/reset/', controller=resetController, action='PUT_INDEX', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_put_key', '/reset/:movie_id', controller=resetController, action='PUT_KEY', conditions=dict(method=['PUT']))

    # ratingsController
    dispatcher.connect('ratings_get_key', '/ratings/:movie_id', controller=ratingsController, action='GET_KEY', conditions=dict(method=['GET']))

    # CORS related OPTIONS endpoints
    dispatcher.connect('movie_key_options', '/movies/:movie_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('movie_options', '/movies/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:movie_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('rating_options', '/ratings/:movie_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))



    # configuration for the server
    conf = {
           'global' : {
               'server.socket_host' : 'student13.cse.nd.edu', # or 'localhost'
               'server.socket_port' : 51022
               },
           '/' : {
                'request.dispatch' : dispatcher,
                'tools.CORS.on' : True
               }
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
